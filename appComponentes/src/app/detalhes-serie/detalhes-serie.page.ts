import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-detalhes-serie',
  templateUrl: './detalhes-serie.page.html',
  styleUrls: ['./detalhes-serie.page.scss'],
})
export class DetalhesSeriePage implements OnInit {
  titulo = "Detalhes das Séries";
  subtitulo = [
    {
      titulo: 'CSI: Miami',
      subtitulo: 'Crime Scene Investigation',
      capa: 'https://flxt.tmsimg.com/assets/p184820_i_h10_ae.jpg',
      temporada: '10'
    }
  ]

  constructor() { }

  ngOnInit() {
  }

}
