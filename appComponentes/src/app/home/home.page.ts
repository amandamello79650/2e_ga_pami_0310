import { Component } from '@angular/core';

@Component({
  selector: 'app-home',
  templateUrl: 'home.page.html',
  styleUrls: ['home.page.scss'],
})
export class HomePage {
 titulo = "Police Series";
 series = [
  {
    titulo: 'Lucifer',
    subtitulo: '',
    capa: 'https://image.ibb.co/hiiOHd/thumb_1920_694600.jpg',
    texto: "Lucifer Morningstar, se cansa da vida demoníaca que leva no subterrâneo e abandona seu trono para tirar férias em Los Angeles. Ele abre uma casa noturna chamada Lux e passa a desfrutar de tudo o que mais gosta: vinho, mulheres e excessos. Sua nova rotina se complica quando sua amiga é assassinada diante de seus olhos. Pela primeira vez em mais de 10 milhões de anos, Lucifer um desejo de justiça e resolve punir os responsáveis pelo crime."
  },
  {
    titulo: "Brooklyn Nine-Nine",
    subtitulo: "",
    capa: "https://streamingsbrasil.com/wp-content/uploads/2022/01/Brooklyn-99-Temp-9-Thumbnail-1130x580.jpg",
    texto: "A série gira em torno de Jake Peralta, um imaturo, mas talentoso, detetive da polícia de Nova York na 99.ª Delegacia do Brooklyn, que muitas vezes entra em conflito com seu novo comandante, o sério e severo capitão Raymond Holt."
  },
  {
    titulo: "Criminal Minds",
    subtitulo: "Unidade de Análise Comportamental",
    capa: "https://multiversonoticias.com.br/wp-content/uploads/2022/02/multiverso-2.jpg",
    texto: "A equipe analisa criminosos do país por meio do modus operandi e a Vitimologia dos mesmos e antecipa seus próximos movimentos antes de eles agirem outra vez. Neste quesito, a série difere-se de outros dramas policias por focar mais no comportamento criminal do suspeito e elaboração de seu perfil (como profiler) do que o crime em si."
  },
  {
    titulo: "Legacy",
    subtitulo: "",
    capa: "https://guiadanetflix.com.br/wp-content/uploads/2018/08/legacies.jpg",
    texto: "Hope Mikaelson, descendente da linhagem mais poderosa de vampiros, lobisomens e bruxas, frequenta a Escola Salvatore para Jovens e Superdotados, um refúgio onde ela pode aprender a controlar suas habilidades e impulsos sobrenaturais."
  },
  {
    titulo: "Dexter",
    subtitulo: "",
    capa: "https://poltronanerd.com.br/wp-content/uploads/2020/06/b28117ea-41bc-47fe-93ef-f82a1f31717b.jpg",
    texto: "Dexter é um especialista forense que passa o dia solucionando crimes e a noite cometendo assassinatos. Inteligente e bonito, o assassino em série vive em conflito com seu instinto de matador e o desejo pela felicidade."
  },
  {
    titulo: "Prison Break",
    subtitulo: "",
    capa: "https://i0.wp.com/okayokayentretenimento.com/wp-content/uploads/2021/12/d5383-prison-break.jpg?fit=2560%2C1920&ssl=1",
    texto: "Michael Scofield é um homem em uma situação desesperada. Seu irmão, Lincoln Burrows, foi condenado por um crime que não cometeu e colocado no corredor da morte. Michael rende um banco para conseguir ser encarcerado junto ao irmão na penitenciária estadual de Fox River e coloca em ação uma série de planos elaborados para permitir a fuga de Lincoln e provar sua inocência. Porém, mesmo fora da prisão seus perigos não acabam."
  },
  {
    titulo: "The Sinner",
    subtitulo: "",
    capa: "https://i.ibb.co/ZWTbsZG/the-sinner-destaque-site.jpg",
    texto: "The Sinner acompanha a jovem mãe Cora que, durante um ataque de raiva inexplicável, comete um ato de violência assustador – e, para seu horror, sem saber por quê. O investigador Harry Ambrose é encarregado de descobrir um motivo, mesmo que esteja enterrado no inconsciente da moça, e acaba desvendando segredos violentos de seu passado."
  },
  {
    titulo: "Quem matou Sara?",
    subtitulo: "",
    capa: "http://www.setedias.com.br//sig/www/openged/conteudos/26427/026427_61968b092c68c_Quemmatousaradivulgacao.jpg",
    texto: "É a história de Alejandro Guzmán, que foi injustamente acusado do assassinato de sua irmã Sara. Depois de 18 anos na prisão, Álex está livre e determinado a fazer justiça com as próprias mãos. Para descobrir a verdade, ele decide se reaproximar da família responsável por incriminá-lo no passado. Mas eles não vão medir esforços para manter seus segredos fora do jogo. Como a morte de Sara aconteceu há quase duas décadas, Álex viaja no tempo para descobrir tudo e, assim, se vingar daqueles que destruíram sua vida."
  }
 ];

  constructor() {}

}
