import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { IonicModule } from '@ionic/angular';
import { FormsModule } from '@angular/forms';
import { HomePage } from './home.page';

import { HomePageRoutingModule } from './home-routing.module';


@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    HomePageRoutingModule
  ],
  declarations: [HomePage]
})
export class HomePageModule {
  titulo = "Police Series";
  series = [
    {
      titulo: 'Lucifer',
      subtitulo: '',
      capa: 'https://image.ibb.co/hiiOHd/thumb_1920_694600.jpg',
      texto: "Lucifer Morningstar, o famigerado Senhor do Inferno, se cansa da vida demoníaca que leva no subterrâneo e abandona seu trono para tirar férias em Los Angeles. Lá, ele abre uma casa noturna chamada Lux e passa a desfrutar de tudo o que mais gosta: vinho, mulheres e excessos. Sua nova rotina se complica quando uma estrela do pop é brutalmente assassinada diante de seus olhos. Pela primeira vez em mais de 10 milhões de anos, Lucifer sente despertar em seu interior um desejo de justiça e resolve punir os responsáveis pelo crime. "
    },
    {
      titulo: "Brooklyn Nine-Nine",
      subtitulo: "",
      capa: "https://streamingsbrasil.com/wp-content/uploads/2022/01/Brooklyn-99-Temp-9-Thumbnail-1130x580.jpg",
      texto:"Brooklyn Nine-Nine (abreviado como B99) é uma série de televisão de comédia policial americana criada por Dan Goor e Michael Schur. A série gira em torno de Jake Peralta (Andy Samberg), um imaturo, mas talentoso, detetive da polícia de Nova York na fictícia 99.ª Delegacia do Brooklyn, que muitas vezes entra em conflito com seu novo comandante, o sério e severo capitão Raymond Holt (Andre Braugher)."
    },
    {
      titulo: "The BlackList",
      subtitulo: "",
      capa: "https://s-cdn.serienjunkies.de/the-blacklist/hdtv.jpg",
      texto:"A série gira em torno de Raymond Red Reddington (James Spader), um ex-oficial da marinha dos Estados Unidos que se tornou um criminoso de alto nível. A história começa a partir da rendição de Raymond ao FBI, na qual está disposto a revelar uma lista de nomes com os criminosos mais perigosos do mundo que ele mesmo criou ao longo dos anos em troca de imunidade aos seus crimes."
    }
  ];
    constructor() {}
}
